# Vetting Vendor Security Claims

The files in this repository are documents meant to help non-technical operations staff in small organizations to think critically about the security claims that their information systems' vendors make. 

There are currently two documents:

[Principles of Secure Systems Architectures](secure_architectures.md) - a non-technical overview of the features and properties of secure systems.

[Assessing Secure Systems Service Providers](questions_for_providers.md) - a set of questions to ask vendors about their systems' security and their security operations.  


These are in no way comprehensive documents nor does their use guarantee that any given vendor meets your particular threat model. 

***If your information has high risk or severe threats associated with it, think about engaging professional assistance in deploying your information systems.***

Eventually we would love to see this become a handbook of sorts and additional documents, edits, pull requests or issues on this repository are welcomed. Contact us at info@iecology.org with questions or opportunities to collaborate!

-----

This content is released under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/) and can be remixed, translated, or amended freely as long as the results are shared in turn and the original documents are attributed to Information Ecology.

![Creative Commons Attribution-ShareAlike 4.0 International License Image](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

See the [license file](LICENSE) for full license terms.
